package net.ruippeixotog.utils.algorithms;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Iterator;
import java.util.Set;
import java.util.Stack;

import net.ruippeixotog.utils.graph.Graph;
import net.ruippeixotog.utils.graph.SimpleGraph;


public class GraphColoring<T> {

	private ArrayList<T> vertexesLeft = new ArrayList<T>();
	private Stack<T> toColor = new Stack<T>();

	public boolean colorGraph(SimpleGraph<T, Integer, ?> graph, int numColors) {

		Graph<T, Integer, ?> workingGraph = graph.clone();
		
		//System.out.println(workingGraph);
		Set<T> vertexes = workingGraph.vertexKeySet();
		toColor.clear();
		vertexesLeft.clear();
		
		boolean success = true;

		// adicionar todos os vertices a pilha
		while (true) {

			boolean changed = true;
			T mostConstrained = null;
			int mostNumEdges = 0;

			// iterar sobre todos os vertices, retirando do grafo e colocando na
			// pilha aqueles com numero de nos inferior ao numero de cores
			while (changed) {
				changed = false;

				Iterator<T> it = vertexes.iterator();
				while (it.hasNext()) {
					T vertex = it.next();
					int numEdges = workingGraph.getNeighbourCount(vertex);
					if (numEdges < numColors) {
						toColor.push(vertex);
						it.remove();
						workingGraph.removeVertex(vertex);
						changed = true;
					} else if (numEdges > mostNumEdges) {
						mostConstrained = vertex;
						mostNumEdges = numEdges;
					}
				}
			}

			// se os vertices foram todos retirados, sair do ciclo
			if (vertexes.isEmpty())
				break;

			// retirar o vertice com mais adjacencias e refazer algoritmo
			success = false;
			vertexesLeft.add(mostConstrained);
			vertexes.remove(mostConstrained);
			workingGraph.removeVertex(mostConstrained);
		}

		// retirar vertices da pilha, atribuindo cores
		while (!toColor.empty()) {
			T vertex = toColor.pop();

			BitSet colors = new BitSet();
			for (T adj : graph.neighbourSet(vertex)) {
				if (graph.getVertex(adj) != null)
					colors.set(graph.getVertex(adj));
			}
			graph.setVertex(vertex, colors.nextClearBit(0));
		}
		
		return success;
	}
	
	public ArrayList<T> getVertexesLeft() {
		return vertexesLeft;
	}
}
