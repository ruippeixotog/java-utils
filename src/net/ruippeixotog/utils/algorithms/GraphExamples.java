package net.ruippeixotog.utils.algorithms;

import java.util.List;

import net.ruippeixotog.utils.graph.Graph;
import net.ruippeixotog.utils.graph.SimpleGraph;
import net.ruippeixotog.utils.meta.IdentityFunc;

public class GraphExamples {

	public static void main(String[] args) {
		DijkstraAlgorithm<Character, ?, Integer> dijkstra = new DijkstraAlgorithm<Character, Object, Integer>(
				distanceGraph(), new IdentityFunc<Integer>());

		List<Character> path = dijkstra.getShortestPath('o', 'h');
		System.out.println(path);
	}

	public static Graph<Character, Object, Integer> distanceGraph() {
		Graph<Character, Object, Integer> graph = new SimpleGraph<Character, Object, Integer>();

		graph.addVertex('o', null);
		for (char c = 'a'; c < 'h'; c++)
			graph.addVertex(c, null);

		graph.addEdge('o', 'a', 8);
		graph.addEdge('o', 'b', 7);
		graph.addEdge('o', 'c', 9);
		graph.addEdge('a', 'b', 6);
		graph.addEdge('a', 'd', 5);
		graph.addEdge('b', 'c', 4);
		graph.addEdge('b', 'd', 5);
		graph.addEdge('b', 'e', 6);
		graph.addEdge('b', 'f', 10);
		graph.addEdge('c', 'e', 5);
		graph.addEdge('c', 'g', 12);
		graph.addEdge('d', 'e', 7);
		graph.addEdge('d', 'f', 6);
		graph.addEdge('e', 'f', 7);
		graph.addEdge('e', 'g', 8);
		graph.addEdge('f', 'g', 6);
		graph.addEdge('f', 'h', 12);
		graph.addEdge('g', 'h', 8);

		return graph;
	}
}
