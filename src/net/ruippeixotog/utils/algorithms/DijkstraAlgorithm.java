package net.ruippeixotog.utils.algorithms;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;

import net.ruippeixotog.utils.graph.Graph;
import net.ruippeixotog.utils.meta.Func;

public class DijkstraAlgorithm<K, V, E> {

	private Graph<K, V, E> graph;
	private Func<? super E, Integer> eval;

	private K to;
	private Map<K, VertexData> vertexData = new HashMap<K, VertexData>();
	private PriorityQueue<K> unvisitedNodes = new PriorityQueue<K>(1,
			new DistanceComparator());

	public DijkstraAlgorithm(Graph<K, V, E> graph,
			Func<? super E, Integer> distanceEval) {
		this.graph = graph;
		eval = distanceEval;
	}

	public Graph<K, V, E> getGraph() {
		return graph;
	}

	public void setGraph(Graph<K, V, E> graph) {
		this.graph = graph;
	}

	public Func<? super E, Integer> getEvaluator() {
		return eval;
	}

	public void setEvaluator(Func<? super E, Integer> eval) {
		this.eval = eval;
	}

	public List<K> getShortestPath(K from, K to) {
		this.to = to;
		vertexData.clear();
		unvisitedNodes.clear();

		rootNodeDiscovered(from);
		while (visitNode(unvisitedNodes.poll()))
			;

		return buildPath();
	}

	private void rootNodeDiscovered(K root) {
		newNodeDiscovered(root, 0, null);
	}

	private void newNodeDiscovered(K node, int distance, K prev) {
		VertexData data = new VertexData(distance, prev);
		vertexData.put(node, data);
		unvisitedNodes.add(node);
		vertexDataChanged(node, data);
	}

	private boolean visitNode(K node) {
		if (node == null || node == to)
			return false;
		beforeVisitNode(node);

		// mark this node as permanent
		VertexData nodeData = vertexData.get(node);
		nodeData.visited = true;

		// update all neighbours' paths
		for (K adj : graph.neighbourSet(node)) {
			VertexData adjData = vertexData.get(adj);

			// if already marked as permanent, continue
			if (adjData != null && adjData.visited)
				continue;

			// if found a new node, add to the priority queue and continue
			E edgeData = graph.getEdge(node, adj);
			int newDistance = nodeData.distance + eval.evaluate(edgeData);
			if (adjData == null) {
				newNodeDiscovered(adj, newDistance, node);
				continue;
			}

			// if not new and the new path is better than the previous, update
			// the priority queue
			int comp = newDistance - adjData.distance;
			if (comp < 0 || comp == 0 && tiebreak(node, adjData.previous, adj)) {
				unvisitedNodes.remove(adj);
				adjData.distance = newDistance;
				adjData.previous = node;
				unvisitedNodes.add(adj);
				vertexDataChanged(adj, adjData);
			}
		}
		return true;
	}

	private List<K> buildPath() {
		List<K> path = new ArrayList<K>();
		K current, previous = to;

		while (previous != null) {
			current = previous;
			path.add(current);
			previous = vertexData.get(current).previous;
		}
		;

		Collections.reverse(path);
		return path.size() < 2 ? null : path;
	}

	protected void beforeVisitNode(K node) {
	}

	protected void vertexDataChanged(K node, VertexData data) {
	}

	protected boolean tiebreak(K newNode, K oldNode, K destination) {
		return false;
	}

	protected class VertexData {
		private int distance;
		private K previous = null;
		private boolean visited = false;

		public VertexData(int distance, K previous) {
			this.distance = distance;
			this.previous = previous;
		}

		public int getDistance() {
			return distance;
		}

		public K getPrevious() {
			return previous;
		}

		public boolean isVisited() {
			return visited;
		}
	}

	private class DistanceComparator implements Comparator<K> {
		@Override
		public int compare(K arg0, K arg1) {
			int comp = vertexData.get(arg1).distance
					- vertexData.get(arg0).distance;
			if (comp != 0)
				return comp;
			return arg0.hashCode() - arg1.hashCode();
		}
	}
}
