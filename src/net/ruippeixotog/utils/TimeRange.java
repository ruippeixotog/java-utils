package net.ruippeixotog.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeRange {
	Date begin;
	Date end;

	static public final long HOUR_MILIS = 3600000;
	public static final DateFormat TIME_FORMAT = new SimpleDateFormat("HH:mm");

	public TimeRange(Date begin, Date end) {
		this.begin = begin;
		this.end = end;
	}

	public Date getBegin() {
		return begin;
	}

	public Date getEnd() {
		return end;
	}

	public Date length() {
		return new Date(end.getTime() - begin.getTime() - HOUR_MILIS);
	}

	public int minutesLength() {
		return (int) (end.getTime() - begin.getTime()) / 60000;
	}

	public boolean hasBegin() {
		return begin != null && begin != end;
	}

	public TimeRange minus(TimeRange time) {

		Date arrivalDiff = hasBegin() ? new Date(begin.getTime()
				- time.end.getTime() - HOUR_MILIS) : null;
		Date departureDiff = new Date(end.getTime() - time.end.getTime()
				- HOUR_MILIS);

		return new TimeRange(arrivalDiff, departureDiff);
	}

	@Override
	public String toString() {
		return (hasBegin() ? TIME_FORMAT.format(begin) + " to " : "")
				+ TIME_FORMAT.format(end);
	}
}
