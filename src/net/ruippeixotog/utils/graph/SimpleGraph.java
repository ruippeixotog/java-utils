package net.ruippeixotog.utils.graph;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.Map.Entry;

/**
 * Implementation of a simple bidirectional graph. Constant time complexity is
 * guaranteed for almost all base operations.
 * 
 * @param <K>
 *            type of the key objects that uniquely identify a vertex.
 * @param <V>
 *            type of the vertex data.
 * @param <E>
 *            type of the edge data.
 */
@SuppressWarnings("serial")
public class SimpleGraph<K, V, E> implements Graph<K, V, E>, Cloneable, Serializable {

	protected HashMap<K, V> vertexData;
	protected HashMap<K, HashMap<K, E>> vertexEdges;

	/**
	 * Constructs a null graph.
	 */
	public SimpleGraph() {
		vertexData = new HashMap<K, V>();
		vertexEdges = new HashMap<K, HashMap<K, E>>();
	}

	/**
	 * Constructs a copy of the given graph.
	 * 
	 * @param original
	 */
	@SuppressWarnings("unchecked")
	public SimpleGraph(SimpleGraph<K, V, E> original) {
		vertexData = (HashMap<K, V>) original.vertexData.clone();
		vertexEdges = new HashMap<K, HashMap<K, E>>();
		for (Entry<K, HashMap<K, E>> entry : original.vertexEdges.entrySet()) {
			vertexEdges.put(entry.getKey(), (HashMap<K, E>) entry.getValue()
					.clone());
		}
	}

	@Override
	public void addVertex(K key, V data) {
		vertexData.put(key, data);
		vertexEdges.put(key, new HashMap<K, E>());
	}

	@Override
	public boolean hasVertex(K key) {
		return vertexData.containsKey(key);
	}

	@Override
	public V getVertex(K key) {
		return vertexData.get(key);
	}

	@Override
	public void setVertex(K key, V data) {
		if (vertexData.containsKey(key))
			vertexData.put(key, data);
	}

	@Override
	public void removeVertex(K key) {
		vertexData.remove(key);
		for (HashMap<K, E> edges : vertexEdges.values())
			edges.remove(key);
	}

	@Override
	public int getVertexCount() {
		return vertexData.size();
	}

	@Override
	public Set<K> vertexKeySet() {
		return vertexEdges.keySet();
	}

	@Override
	public void addEdge(K source, K dest) {
		addEdge(source, dest, null);
	}

	@Override
	public void addEdge(K source, K dest, E data) {
		vertexEdges.get(source).put(dest, data);
	}

	@Override
	public boolean hasEdge(K source, K dest) {
		return vertexEdges.get(source).containsKey(dest);
	}

	@Override
	public E getEdge(K source, K dest) {
		return vertexEdges.get(source).get(dest);
	}

	@Override
	public void setEdge(K source, K dest, E data) {
		if (vertexEdges.get(source).containsKey(dest))
			vertexEdges.get(source).put(dest, data);
	}

	@Override
	public void removeEdge(K source, K dest) {
		vertexEdges.get(source).remove(dest);
	}

	@Override
	public int getNeighbourCount(K source) {
		return vertexEdges.get(source).size();
	}

	@Override
	public Set<K> neighbourSet(K source) {
		return vertexEdges.get(source).keySet();
	}

	@Override
	public void clear() {
		vertexData.clear();
		vertexEdges.clear();
	}

	/**
	 * Returns a clone of the structure of this graph. All vertexes and edges of
	 * the graph are present in the returned graph, but all vertex and edge data
	 * are set to null.
	 * 
	 * @param <V2>
	 *            type of the returned graph vertex data.
	 * @param <E2>
	 *            type of the returned graph edge data.
	 * @return a clone of the structure of this graph.
	 */
	public <V2, E2> SimpleGraph<K, V2, E2> structClone() {
		return structClone(null, null);
	}

	/**
	 * Returns a clone of the structure of this graph. All vertexes and edges of
	 * the graph are present in the returned graph as well as all edge data, but
	 * all vertex data is set to the given default value.
	 * 
	 * @param <V2>
	 *            type of the returned graph vertex data.
	 * @param vertexValue
	 *            default value for vertex data.
	 * @return a clone of the structure of this graph.
	 */
	@SuppressWarnings("unchecked")
	public <V2> SimpleGraph<K, V2, E> structClone(V2 vertexValue) {
		SimpleGraph<K, V2, E> copy = new SimpleGraph<K, V2, E>();
		for (K key : vertexData.keySet())
			copy.vertexData.put(key, vertexValue);
		for (Entry<K, HashMap<K, E>> entry : vertexEdges.entrySet()) {
			copy.vertexEdges.put(entry.getKey(), (HashMap<K, E>) entry
					.getValue().clone());
		}
		return copy;
	}

	/**
	 * Returns a clone of the structure of this graph. All vertexes and edges of
	 * the graph are present in the returned graph, but all vertex and edge data
	 * are set to their given default values.
	 * 
	 * @param <V2>
	 *            type of the returned graph vertex data.
	 * @param <E2>
	 *            type of the returned graph edge data.
	 * @param vertexValue
	 *            default value for vertex data.
	 * @param edgeValue
	 *            default value for edge data.
	 * @return a clone of the structure of this graph.
	 */
	public <V2, E2> SimpleGraph<K, V2, E2> structClone(V2 vertexValue, E2 edgeValue) {
		SimpleGraph<K, V2, E2> copy = new SimpleGraph<K, V2, E2>();
		for (K key : vertexData.keySet())
			copy.vertexData.put(key, vertexValue);
		for (K key : vertexEdges.keySet()) {
			HashMap<K, E2> copyEdges = new HashMap<K, E2>();
			copy.vertexEdges.put(key, copyEdges);

			HashMap<K, E> edges = vertexEdges.get(key);
			for (K edge : edges.keySet())
				copyEdges.put(edge, edgeValue);
		}
		return copy;
	}

	/**
	 * Returns a shallow copy of this graph.
	 * 
	 * @return a shallow copy of this graph.
	 */
	@Override
	public SimpleGraph<K, V, E> clone() {
		return new SimpleGraph<K, V, E>(this);
	}

	/**
	 * Returns a string representation of this graph.
	 */
	@Override
	public String toString() {

		StringBuffer ret = new StringBuffer();
		for (K key : vertexData.keySet()) {
			ret.append("[" + key + "] => [" + vertexData.get(key) + "] : "
					+ "{");
			if (!vertexEdges.get(key).isEmpty()) {
				Iterator<Entry<K, E>> it = vertexEdges.get(key).entrySet()
						.iterator();
				Entry<K, E> edge = it.next();
				ret.append(edge.getKey());
				if (edge.getValue() != null)
					ret.append("[" + edge.getValue() + "]");
				while (it.hasNext()) {
					edge = it.next();
					ret.append(", " + edge.getKey());
					if (edge.getValue() != null)
						ret.append("[" + edge.getValue() + "]");
				}
			}
			ret.append("}\n");
		}
		return ret.toString();
	}
}