package net.ruippeixotog.utils.graph;

import java.util.Set;

/**
 * Implementation of a simple bidirectional graph. Constant time complexity is
 * guaranteed for almost all base operations.
 * 
 * @param <K>
 *            type of the key objects that uniquely identify a vertex.
 * @param <V>
 *            type of the vertex data.
 * @param <E>
 *            type of the edge data.
 */
public interface Graph<K, V, E> {

	/**
	 * Adds a vertex to this graph.
	 * 
	 * @param key
	 *            key of the vertex to add.
	 * @param data
	 *            vertex data.
	 */
	void addVertex(K key, V data);

	/**
	 * Returns true if the given vertex exists in this graph.
	 * 
	 * @param key
	 *            key of the vertex.
	 * @return true if the given vertex exists in this graph.
	 */
	boolean hasVertex(K key);

	/**
	 * Returns the data of the given vertex.
	 * 
	 * @param key
	 *            key of the vertex.
	 * @return the data of the given vertex.
	 */
	V getVertex(K key);

	/**
	 * Sets the data of the given vertex.
	 * 
	 * @param key
	 *            key of the vertex.
	 * @param data
	 *            vertex data.
	 */
	void setVertex(K key, V data);

	/**
	 * Removes the given vertex from this graph.
	 * 
	 * @param key
	 *            key of the vertex.
	 */
	void removeVertex(K key);

	/**
	 * Returns the number of vertexes in this graph.
	 * 
	 * @return the number of vertexes in this graph.
	 */
	int getVertexCount();

	/**
	 * Returns a set containing the vertexes of this graph.
	 * 
	 * @return a set containing the vertexes of this graph.
	 */
	Set<K> vertexKeySet();

	/**
	 * Adds an edge with null data bewteen two vertexes.
	 * 
	 * @param source
	 *            source vertex.
	 * @param dest
	 *            destination vertex.
	 */
	void addEdge(K source, K dest);

	/**
	 * Adds an edge with the given edge data bewteen two vertexes.
	 * 
	 * @param source
	 *            source vertex.
	 * @param dest
	 *            destination vertex.
	 * @param data
	 *            edge data.
	 */
	void addEdge(K source, K dest, E data);

	/**
	 * Returns true if an edge bewteen the given vertexes exist.
	 * 
	 * @param source
	 *            source vertex.
	 * @param dest
	 *            destination vertex.
	 * @return true if an edge bewteen the given vertexes exist.
	 */
	boolean hasEdge(K source, K dest);

	/**
	 * Returns the data of the edge between the given vertexes.
	 * 
	 * @param source
	 *            source vertex.
	 * @param dest
	 *            destination vertex.
	 * @return the data of the edge between the given vertexes.
	 */
	E getEdge(K source, K dest);

	/**
	 * Sets the data of the edge between the given vertexes.
	 * 
	 * @param source
	 *            source vertex.
	 * @param dest
	 *            destination vertex.
	 * @param data
	 *            data of the edge.
	 */
	void setEdge(K source, K dest, E data);

	/**
	 * Removes an edge between the given vertexes.
	 * 
	 * @param source
	 *            source vertex.
	 * @param dest
	 *            destination vertex.
	 */
	void removeEdge(K source, K dest);

	/**
	 * Returns the number of adjacencies of the given vertex.
	 * 
	 * @param source
	 *            source vertex.
	 * @return the number of adjacencies of the given vertex.
	 */
	int getNeighbourCount(K source);

	/**
	 * Returns a set of the adjacent vertexes of a given vertex.
	 * 
	 * @param source
	 *            source vertex.
	 * @return a set of the adjacent vertexes of a given vertex.
	 */
	Set<K> neighbourSet(K source);

	/**
	 * Removes all edges and vertexes from this graph.
	 */
	void clear();
}