package net.ruippeixotog.utils.graph;

/**
 * Implementation of a simple unidirectional graph. Constant time complexity is
 * guaranteed for almost all base operations.
 * 
 * @param <K>
 *            type of the key objects that uniquely identify a vertex.
 * @param <V>
 *            type of the vertex data.
 * @param <E>
 *            type of the edge data.
 */
@SuppressWarnings("serial")
public class UndirectedGraph<K, V, E> extends SimpleGraph<K, V, E> {

	/**
	 * Constructs a null graph.
	 */
	public UndirectedGraph() {
		super();
	}

	/**
	 * Constructs a copy of the given graph.
	 * 
	 * @param original
	 */
	public UndirectedGraph(UndirectedGraph<K, V, E> original) {
		super(original);
	}

	@Override
	public void addEdge(K source, K dest) {
		addEdge(source, dest, null);
	}

	@Override
	public void addEdge(K source, K dest, E data) {
		vertexEdges.get(source).put(dest, data);
		vertexEdges.get(dest).put(source, data);
	}

	@Override
	public void setEdge(K source, K dest, E data) {
		if (vertexEdges.get(source).containsKey(dest)) {
			vertexEdges.get(source).put(dest, data);
			vertexEdges.get(dest).put(source, data);
		}
	}

	@Override
	public void removeEdge(K source, K dest) {
		vertexEdges.get(source).remove(dest);
		vertexEdges.get(dest).remove(source);
	}

	@Override
	public UndirectedGraph<K, V, E> clone() {
		return new UndirectedGraph<K, V, E>(this);
	}
}
