package net.ruippeixotog.utils.meta;

/**
 * Representation of the identity function, mapping objects to themselves.
 * 
 * @author Rui Gonçalves
 * 
 * @param <T>
 *            Domain of this function.
 */
public class IdentityFunc<T> implements Func<T, T> {

	/**
	 * Maps a object to itself.
	 * 
	 * @param obj
	 *            the object to be mapped.
	 * @return The received parameter.
	 */
	@Override
	public T evaluate(T obj) {
		return obj;
	}
}
