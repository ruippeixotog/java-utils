package net.ruippeixotog.utils.meta;

/**
 * Representation of a function mapping objects to a numerical value. Commonly
 * used to calculate the value or utility of states or solutions in certain
 * algorithms.
 * 
 * @author Rui Gonçalves
 * 
 * @param <T>
 *            type of the objects to be evaluated.
 */
public interface Evaluator<T> {
	/**
	 * Maps a given object to a numerical value.
	 * 
	 * @param obj
	 *            object to be evaluated.
	 * @return the value of the given object.
	 */
	double evaluate(T obj);
}
