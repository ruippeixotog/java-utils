package net.ruippeixotog.utils.meta;

/**
 * A simple representation of a function mapping objects to other objects.
 * 
 * @author Rui Gonçalves
 * 
 * @param <T>
 *            Domain of this function.
 * @param <U>
 *            Return type of this funcion.
 */
public interface Func<T, U> {
	/**
	 * Maps a given object to another.
	 * 
	 * @param obj
	 *            the object to be mapped.
	 * @return the returned object.
	 */
	U evaluate(T obj);
}
