package net.ruippeixotog.utils.net;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

import net.ruippeixotog.utils.io.ByteCountInputStream;


public class HttpClient {

	private Map<String, String> cookies = new HashMap<String, String>();
	private Writer out;
	private long trafficDown = 0;

	public HttpClient() {
		out = new OutputStreamWriter(System.out);
	}

	public HttpClient(Writer out) {
		setWriter(out);
	}

	public void get(String url) throws IOException {

		System.out.println("GET: " + url);

		// create and open url connection for reading
		URL urlObj = new URL(url);
		URLConnection conn = urlObj.openConnection();

		// set existing cookies
		conn.setRequestProperty("Cookie", getCookieString(url));

		// connect
		conn.connect();

		// loop through response headers to set new cookies
		setCookies(conn.getHeaderFields().get("Set-Cookie"));

		// read page
		ByteCountInputStream urlIn = new ByteCountInputStream(conn.getInputStream());
		Scanner sc = new Scanner(urlIn);
		while (sc.hasNextLine())
			out.write(sc.nextLine());
		sc.close();
		trafficDown += urlIn.numBytesRead();
	}

	public void get(String url, Map<String, String> params) throws IOException {
		get(url + "?" + getParamString(params));
	}

	public void post(String url, Map<String, String> params) throws IOException {

		System.out.println("POST: " + url);
		System.out.println("Params: " + getParamString(params));

		// create and open url connection for reading and writing
		URL urlObj = new URL(url);
		URLConnection conn = urlObj.openConnection();
		conn.setDoOutput(true);

		// set existing cookies
		conn.setRequestProperty("Cookie", getCookieString(url));

		// send cookies
		PrintStream post = new PrintStream(conn.getOutputStream());
		post.print(getParamString(params));

		// connect
		conn.connect();

		// loop through response headers to set new cookies
		setCookies(conn.getHeaderFields().get("Set-Cookie"));

		// read page
		Scanner sc = new Scanner(conn.getInputStream());
		while (sc.hasNextLine())
			out.write(sc.nextLine());
	}
	
	public String getCookieContent(String key) {
		return cookies.get(key);
	}

	private void setCookies(List<String> cookieList) {
		if (cookieList == null)
			return;
		try {
			for (String cookie : cookieList) {
				String[] entries = cookie.split("; ");
				// for (String entry : entries) {
				String[] pair = entries[0].split("=", 2);
				cookies.put(URLDecoder.decode(pair[0], "UTF-8"),
						URLDecoder.decode(pair[1], "UTF-8"));

				System.out.println("Cookie received: "
						+ URLDecoder.decode(pair[0], "UTF-8") + "="
						+ URLDecoder.decode(pair[1], "UTF-8"));
				// }
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void setWriter(Writer out) {
		this.out = out;
	}

	public Writer getWriter() {
		return out;
	}
	
	public long numBytesDownloaded() {
		return trafficDown;
	}

	private String getCookieString(String url) {
		return getMapString(cookies, "=", "; ");
	}

	private String getParamString(Map<String, String> params) {
		return getMapString(params, "=", "&");
	}

	private String getMapString(Map<String, String> map, String keyValSep,
			String entrySep) {
		StringBuffer strBuf = new StringBuffer();
		try {
			for (Entry<String, String> c : map.entrySet())
				strBuf.append(URLEncoder.encode(c.getKey(), "UTF-8")
						+ keyValSep + URLEncoder.encode(c.getValue(), "UTF-8")
						+ entrySep);
			if (strBuf.length() > 0)
				strBuf.delete(strBuf.length() - entrySep.length(),
						Integer.MAX_VALUE);
		} catch (UnsupportedEncodingException e) {
			throw new AssertionError();
		}
		return strBuf.toString();
	}
}
