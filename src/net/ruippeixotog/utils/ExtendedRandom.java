package net.ruippeixotog.utils;

import java.util.Random;

/**
 * Utility class that extends the {@link java.util.Random Random} class with
 * more methods for returning random data using the superclass random number
 * generator. It focuses on providing probabilistic methods that return data
 * based on provided probability values.
 * 
 * @author Rui Gonçalves
 */
public class ExtendedRandom extends Random {

	private static final long serialVersionUID = 8881359876415852861L;

	/**
	 * Returns true with the given probability.
	 * 
	 * @param prob
	 *            probability of this method returning true.
	 * @return true with the given probability.
	 */
	public boolean nextBoolean(double prob) {
		return nextDouble() < prob;
	}

	/**
	 * Returns an integer based on the given probability values. More formally,
	 * the probability of this method returning the integer i is probs[i]. As
	 * such, it returns an integer between 0 and probs.length.
	 * 
	 * Ideally, the sum of the probabilities in the array should be 1.0. If it
	 * is less than 1.0, the last element is given the remaining probability. If
	 * it is more, the last elements of the probability array are possibly
	 * truncated and the last element is taken the probability excess.
	 * 
	 * @param probs
	 *            probability values of each integer.
	 * @return an integer based on the given probability values.
	 */
	public int nextInt(double[] probs) {
		double prob = 0.0, test = nextDouble();
		for (int i = 0; i < probs.length; i++) {
			prob += probs[i];
			if (test < prob)
				return i;
		}
		return probs.length - 1;
	}

	/**
	 * Returns a random element of the given array.
	 * 
	 * @param <T>
	 *            the type of the array elements.
	 * @param arr
	 *            the array containing the elements to choose from.
	 * @return a random element of the given array.
	 */
	public <T> T nextElement(T[] arr) {
		if (arr == null || arr.length == 0)
			throw new IllegalArgumentException("null or empty array");
		return arr[nextInt(arr.length)];
	}

	/**
	 * Returns a random element of the given integer array.
	 * 
	 * @param arr
	 *            the array containing the elements to choose from.
	 * @return a random element of the given array.
	 * @throws IllegalArgumentException
	 *             if the array is null or empty.
	 */
	public int nextElement(int[] arr) {
		if (arr == null || arr.length == 0)
			throw new IllegalArgumentException("null or empty array");
		return arr[nextInt(arr.length)];
	}

	/**
	 * Returns an element of an array based on the given probability values.
	 * More formally, the probability of this method returning the element
	 * arr[i] is probs[i].
	 * 
	 * Ideally, the sum of the probabilities in the array should be 1.0. If it
	 * is less than 1.0, the last element is given the remaining probability. If
	 * it is more, the last elements of the probability array are possibly
	 * truncated and the last element is taken the probability excess.
	 * 
	 * @param <T>
	 *            the type of the array elements.
	 * @param arr
	 *            the array containing the elements to choose from.
	 * @param probs
	 * @return an element of the given array, based on the given probability
	 *         values.
	 * @throws IllegalArgumentException
	 *             if one of the given arrays is null or empty or if they are of
	 *             different sizes.
	 */
	public <T> T nextElement(T[] arr, double[] probs) {
		if (arr == null || probs == null || arr.length == 0
				|| arr.length != probs.length)
			throw new IllegalArgumentException("null array or invalid size");
		return arr[nextInt(probs)];
	}
}
