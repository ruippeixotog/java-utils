package net.ruippeixotog.utils.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Implementation of a thread that continually reads data from an InputStream,
 * writing it to an OutputStream.
 * 
 * @author Rui Gonçalves
 */
public class CatThread extends Thread {

	static private final int BUF_SIZE = 512;
	private TeeInputStream tin;

	/**
	 * Constructs a CatThread linking the given InputStream to stdout.
	 * 
	 * @param in
	 *            stream for reading.
	 */
	public CatThread(InputStream in) {
		this(in, System.out);
	}

	/**
	 * Constructs a CatThread linking the given streams.
	 * 
	 * @param in
	 *            stream for reading.
	 * @param out
	 *            stream for writing.
	 */
	public CatThread(InputStream in, OutputStream out) {
		tin = new TeeInputStream(in, out);
	}

	@Override
	public void run() {
		byte[] buffer = new byte[BUF_SIZE];
		try {
			while (tin.read(buffer) != -1) {
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
