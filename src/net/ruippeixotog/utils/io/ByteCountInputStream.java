package net.ruippeixotog.utils.io;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * A class that wraps an existing InputStream, providing a counter for the
 * number of bytes read.
 * 
 * @author Rui Gonçalves
 */
public class ByteCountInputStream extends FilterInputStream {

	private long counter = 0;

	/**
	 * Constructs a ByteCountInputStream.
	 * 
	 * @param in
	 *            stream for reading.
	 */
	public ByteCountInputStream(InputStream in) {
		super(in);
	}

	@Override
	public int read() throws IOException {
		int by = in.read();
		counter++;
		return by;
	}

	@Override
	public int read(byte[] b) throws IOException {
		return read(b, 0, b.length);
	}

	@Override
	public int read(byte[] b, int off, int len) throws IOException {
		int realLen = in.read(b, off, len);
		counter += realLen;
		return realLen;
	}

	/**
	 * Returns the number of bytes read through this stream.
	 * 
	 * @return number of bytes read through this stream.
	 */
	public long numBytesRead() {
		return counter;
	}
}
