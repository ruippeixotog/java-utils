package net.ruippeixotog.utils.io;

import java.io.IOException;
import java.io.OutputStream;

/**
 * A dummy OutputStream that discards the written bytes.
 * 
 * @author Rui Gonçalves
 * 
 */
public class NullOutputStream extends OutputStream {

	@Override
	public void write(int b) throws IOException {	
	}
}
