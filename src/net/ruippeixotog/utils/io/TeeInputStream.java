package net.ruippeixotog.utils.io;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * A class that wraps an existing InputStream, writing all bytes read through it
 * to an auxiliary OutputStream. It is useful for logging data in a transparent
 * way or for retrieving data to multiple threads or devices.
 * 
 * @author Rui Gonçalves
 */
public class TeeInputStream extends FilterInputStream {

	private InputStream in;
	private OutputStream log;

	/**
	 * Constructs a TeeInputStream.
	 * 
	 * @param in
	 *            stream for reading data.
	 * @param log
	 *            stream for outputting read data.
	 */
	public TeeInputStream(InputStream in, OutputStream log) {
		super(in);
		this.in = in;
		this.log = log;
	}

	@Override
	public int read() throws IOException {
		int by = in.read();
		if (by != -1)
			log.write(by);
		return by;
	}

	@Override
	public int read(byte[] b) throws IOException {
		return read(b, 0, b.length);
	}

	@Override
	public int read(byte[] b, int off, int len) throws IOException {
		int realLen = in.read(b, off, len);
		if (realLen > 0)
			log.write(b, 0, realLen);
		return realLen;
	}
}
