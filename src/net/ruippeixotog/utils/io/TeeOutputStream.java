package net.ruippeixotog.utils.io;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * A class that wraps two existing OutputStream. All bytes written through this
 * class are written to both composing streams. It is useful for forking data in
 * a transparent way or for sending data to multiple threads or devices.
 * 
 * @author Rui Gonçalves
 */
public class TeeOutputStream extends FilterOutputStream {

	private OutputStream out1;
	private OutputStream out2;

	/**
	 * Constructs a TeeOutputStream.
	 * 
	 * @param out1
	 *            one of the streams.
	 * @param out2
	 *            the other stream.
	 */
	public TeeOutputStream(OutputStream out1, OutputStream out2) {
		super(out1);
		this.out1 = out1;
		this.out2 = out2;
	}

	public void write(int b) throws IOException {
		out1.write(b);
		out2.write(b);
	}

	public void write(byte[] data, int offset, int length) throws IOException {
		out1.write(data, offset, length);
		out2.write(data, offset, length);
	}

	public void flush() throws IOException {
		out1.flush();
		out2.flush();
	}

	public void close() throws IOException {
		out1.close();
		out2.close();
	}
}
